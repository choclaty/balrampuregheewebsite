﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace puredesighee.Models
{
    public class Enquiry
    {
        public string id { get; set; }
        [Required(ErrorMessage = "Please Enter Name")]
        [MaxLength(100, ErrorMessage = "Name must be between 1 to 100 charachter accept only")]
        public string customername { get; set; }
        public string email { get; set; }
        public string mobileno { get; set; }
        public string subject { get; set; }
        public string remark { get; set; }
        public string cusid { get; set; }

    }
}