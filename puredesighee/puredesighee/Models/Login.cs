﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace puredesighee.Models
{
    public class Login
    {
        [Required(ErrorMessage = "Please Enter Mobile Number or Email Id")]
        public string mobile { get; set; }

        [DataType(DataType.Password)]
        public string userpwd { get; set; }

        public string username { get; set; }

        public int? userid { get; set; }
        public string type { get; set; }
    }

    public class Forgotpwd
    {
        [Required(ErrorMessage = "Please Enter Email")]
        [RegularExpression("([_a-zA-Z0-9-]+)(.[_a-zA-Z0-9-]+)*@([a-zA-Z]+.)+([a-zA-Z]{2,3})", ErrorMessage = "Invalid E-mail Id")]
        public string email { get; set; }
    }

    public class VarifyOtp
    {
        [Required(ErrorMessage = "Please Enter Otp")]
        public string otp { get; set; }

        public string mobileno { get; set; }

    }
}