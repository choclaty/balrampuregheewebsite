﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace puredesighee.Models
{
    public class ProductItems
    {
        public string itemid { get; set; }
        public string itemcatid { get; set; }
        public string itemdesc { get; set; }
        public string ingredients { get; set; }
        public string nutritioninfo { get; set; }
        public string itembarcode { get; set; }
        public string itemname { get; set; }                    
        public string itmimgename { get; set; }                    
        public string discper { get; set; }
        public string salerate { get; set; }
        public string discrate { get; set; }
        public string sgstper { get; set; }
        public string cgstper { get; set; }
        public string igstper { get; set; }
        public string itemwt { get; set; }
        public string unitname { get; set; }
        public string validfor { get; set; }
        public string favitem { get; set; }
        public string taxtype { get; set; }
        public string itemqty { get; set; }
        public string review { get; set; }
        public string custid { get; set; }

    }

}