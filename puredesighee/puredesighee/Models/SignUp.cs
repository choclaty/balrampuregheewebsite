﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace puredesighee.Models
{
    public class SignUp
    {
        public string id { get; set; }
        [Required(ErrorMessage ="Please Enter Name")]
        [MaxLength(100,ErrorMessage ="Name must be between 1 to 100 charachter accept only")]
        public string customername { get; set; }
        [Required(ErrorMessage = "Please Enter Password")]
        [DataType(DataType.Password)]

        public string password { get; set; }
        [Required(ErrorMessage = "Please Enter Confrim Password")]
        [Compare("password", ErrorMessage = "Confirm password doesn't match, Type again !")]
        [DataType(DataType.Password)]

        public string confrimpassword { get; set; }
        [Required(ErrorMessage = "Please Enter Mobile Number")]
        [MaxLength(10,ErrorMessage ="Mobile Number must be 10 digits only")]
        [RegularExpression("^[7-9][0-9]{9}$", ErrorMessage ="Only accept between 6 to 9 start with frist later")]
        public string mobileno { get; set; }
        [Required(ErrorMessage = "Please Enter Email Id")]
        [RegularExpression("([_a-zA-Z0-9-]+)(.[_a-zA-Z0-9-]+)*@([a-zA-Z]+.)+([a-zA-Z]{2,3})", ErrorMessage = "Invalid E-mail Id")]

        public string emailid { get; set; }

        public string address { get; set; }
        public string zipcode { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string state { get; set; }

    }

    public class AddAddress 
    {
        public string id { get; set; }
        public string name { get; set; }
        public string mobileno { get; set; }
        public string flatno { get; set; }
        public string address { get; set; }
        public string defaddress { get; set; }        
        public string zipcode { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string state { get; set; }
        public string type { get; set; }
    }
}