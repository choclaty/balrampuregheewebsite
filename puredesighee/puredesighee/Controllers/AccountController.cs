﻿using puredesighee.DataAccessLayer;
using puredesighee.filter;
using puredesighee.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace puredesighee.Controllers
{
    public class AccountController : Controller
    {
        Account account = new Account();
        AddressManage addressManage = new AddressManage();
        // GET: Account
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Login login)
        {

            var isValideRegEmail = new Regex(@"([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z]+\.)+([a-zA-Z]{2,3})");
            bool isValidEmail = isValideRegEmail.IsMatch(login.mobile);

            var isValideRegMo = new Regex(@"^[6-9]\d{9}$");
            bool isValidMo = isValideRegMo.IsMatch(login.mobile);

            if (isValidMo == false && isValidEmail == false)
            {
                TempData["Error"] = "Invalid User Name  !";
                return View();
            }
            if (login.type == "pwd" && login.userpwd == "")
            {
                TempData["Error"] = "Please enter valid password  !";

                return View();
            }



            if (ModelState.IsValid)
            {

                var valid = account.GetLoginData(login.mobile, login.userpwd);
                if (valid.userid != null)
                {
                    if (login.userpwd != null && login.userpwd != "")
                    {
                        Session["UserID"] = valid.userid;

                        if (Session["ItemId"] != null)
                        {
                            return RedirectToAction("SingleProduct", "Product", new { itemId = Convert.ToString(Session["ItemId"]) });
                        }
                        if (Session["BuyNow"] != null)
                        {
                            return RedirectToAction("CheckOut", "Product");
                        }
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        var status = account.sendOTP(login);
                        if (status == "1")
                        {
                            TempData["Login"] = login;
                            return RedirectToAction("sendOtp", "Account");
                        }
                    }
                }
                else
                {
                    TempData["Error"] = "Invalid Credentials Please Try Again !";
                }

            }
            return View();
        }

        public ActionResult SignUp()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SignUp(SignUp signUp)
        {
            if (ModelState.IsValid)
            {
                var statusCode = account.SaveSignUP(signUp);
                if (statusCode == "1")
                {
                    TempData["Message"] = "Your Registration Successfully";
                    ModelState.Clear();
                }
                else if (statusCode == "2")
                {
                    TempData["Error"] = "Your Mobile No is Exit";
                }
                else if (statusCode == "0")
                {
                    TempData["Error"] = "Your Email id is Exit";
                }
            }
            return View();
        }

        [CheckSession]
        public ActionResult userprofile()
        {
            try
            {
                SignUp sign = new SignUp();
                sign = account.GetUserDetails(Session["UserID"].ToString());
                return View(sign);

            }
            catch (Exception e)
            {
                throw;
            }
        }

        [CheckSession]
        [HttpPost]
        public ActionResult userprofile(SignUp signup)
        {
            try
            {
                ModelState.Remove("password");
                ModelState.Remove("confrimpassword");
                if (ModelState.IsValid)
                {
                    if (Session["UserID"] != null && signup.id == null)
                        signup.id = Session["UserID"].ToString();
                    var statusCode = account.userProfileUpdate(signup);
                    if (statusCode == "1")
                    {
                        TempData["Message"] = "Your Profile Updated Successfully";
                        ModelState.Clear();
                        // return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        TempData["Error"] = "Your Email Id is Exit";
                    }
                }
                return View();


            }
            catch (Exception e)
            {
                throw;
            }
        }

        [CheckSession]
        public ActionResult Signout()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForgotPassword(Forgotpwd forgotpwd)
        {
            var status = account.SendForgotPasswordNotification(forgotpwd.email);
            if (status == "1")
            {
                TempData["success"] = "Send Password to Your email ! ";
                ModelState.Clear();
            }
            else
            {
                TempData["err"] = "Can not Reset Password try agin ! ";
            }
            return View();
        }

        public ActionResult sendOtp()
        {
            if (ModelState.IsValid)
            {
                VarifyOtp verify = new VarifyOtp();
                var login = TempData["Login"] as Login;
                verify.mobileno = login.mobile;
                return View(verify);
            }
            else
            {
                TempData["Error"] = "Please Enter OTP ";
                return View();
            }
        }

        [HttpPost]
        public ActionResult sendOtp(VarifyOtp votp)
        {
            var validTime = account.VarifyOTP(votp.otp, votp.mobileno);
            var currentTime = DateTime.Now;
            DateTime timeValid = Convert.ToDateTime(validTime).AddMinutes(10);

            //for Otp time server and local dif issue
            // if ( timeValid <=currentTime)
            {
                var loginuser = account.GetLoginData(votp.mobileno, "");

                if (loginuser.userid != null)
                {
                    Session["UserID"] = loginuser.userid;

                    if (Session["ItemId"] != null)
                    {
                        return RedirectToAction("SingleProduct", "Product", new { itemId = Convert.ToString(Session["ItemId"]) });
                    }
                    if (Session["BuyNow"] != null)
                    {
                        return RedirectToAction("CheckOut", "Product");
                    }
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    TempData["Error"] = "Invalid OTP Please Try Again !";
                }
            }
            // else
            {
                //  TempData["Error"] = "Invalid OTP Please Try Again !";
            }

            return View();
        }

        public JsonResult ResendOTP(string mobileno)
        {

            Login login = new Login();
            login.mobile = mobileno;
            var status = account.sendOTP(login);
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        //for menage address

        public ActionResult Address()
        {
            
            List<AddAddress> items = new List<AddAddress>();

            items = addressManage.GetAddressList(Convert.ToString(Session["UserID"]));
            return View(items);
        }

        [HttpPost]
        public ActionResult Address(FormCollection collection)
        {
            //zero insert and one update
            string callType = "0";
            AddAddress address = new AddAddress();
            var id = collection["ids"];
            if (id == null || id == "")
                callType = "0";
            else
            {
                callType = "1";
                address.id = collection["ids"];
            }
            address.type = collection["type"];
            address.name = collection["name"];
            address.mobileno = collection["mobileno"];
            address.country = collection["country"];
            address.state = collection["state"];
            address.city = collection["city"];
            address.zipcode = collection["zipcode"];
            address.address = collection["address"];
            //For address add=0 flag
            addressManage.SaveAddress(address, Convert.ToString(Session["UserId"]), Convert.ToInt32(callType));
            var checkout = collection["checkout"];
            if (checkout != null && checkout != "")
                return RedirectToAction("CheckOut", "Product");
            else
                return RedirectToAction("Address");
        }

        public JsonResult getAddress(string id)
        {
            AddAddress item = new AddAddress();
            var items = addressManage.GetAddressList(Convert.ToString(Session["UserID"]));
            item = items.Find(x => x.id == id);
            return Json(item, JsonRequestBehavior.AllowGet);

        }

        public JsonResult deleteAddres(string id)
        {
            var status = addressManage.DeleteAddressFindById(Convert.ToString(Session["UserID"]), id);
            return Json(status, JsonRequestBehavior.AllowGet);
        }
    }
}