﻿using puredesighee.DataAccessLayer;
using puredesighee.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace puredesighee.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View("About");
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Gallery()
        {
            return View("Gallery");
        }

        public ActionResult NotFound()
        {
            return View();
        }

        public ActionResult Team()
        {
            return View();
        }

        public ActionResult OurService()
        {
            return View();
        }

        public ActionResult ComingSoon()
        {
            return View();
        }

        public ActionResult Objective()
        {
            return View();
        }

        public ActionResult VisionMission()
        {
            return View();
        }

        public ActionResult Certificate()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Contact(Enquiry enquiry)
        {
            if (ModelState.IsValid)
            {
                Home home = new Home();
                enquiry.cusid = Convert.ToString(Session["UserID"]);
                var statusCode = home.SaveEnquiry(enquiry);
                if (statusCode == "")
                {
                    TempData["Message"] = "Your Enquiry Submited Successfully";
                    ModelState.Clear();
                }
                else
                {
                    TempData["Error"] = "Somthing want to worng ! try agin";
                }
            }
            return View();
        }

        public ActionResult Content()
        {
            return View();
        }
    }
}