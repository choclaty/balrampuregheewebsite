﻿using puredesighee.DataAccessLayer;
using puredesighee.filter;
using puredesighee.Models;
using System;
using System.Collections.Generic;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace puredesighee.Controllers
{
    public class ProductController : Controller
    {
        clsencrdecr clsencr = new clsencrdecr();
        AddressManage addressManage = new AddressManage();
        // GET: Product

        double qtypric = 500.00;
        public ActionResult Products()
        {
            Product product = new Product();
            List<ProductItems> itemsList = new List<ProductItems>();
            try
            {
                itemsList = product.GetItems();
            }
            catch (Exception e)
            {
                throw;
            }
            return View(itemsList);
        }

        public ActionResult SingleProduct(string itemId, int? buyNow)
        {
            if (itemId != null)
            {
                if (buyNow == null)
                {
                    Session["ItemId"] = itemId;
                    Session["BuyNow"] = null;
                }
                else
                {
                    Session["BuyNow"] = itemId;
                    Session["ItemId"] = null;
                }
            }
            if (Session["UserId"] == null)
            {
                return RedirectToAction("Login", "Account");
            }
            Product product = new Product();
            ProductItems item = new ProductItems();
            try
            {
                if (Session["ItemId"] != null)
                {
                    itemId = Convert.ToString(Session["ItemId"]);
                    Session["ItemId"] = null;
                }
                if (Session["BuyNow"] != null)
                {
                    return RedirectToAction("CheckOut", "Product");
                }
                item = product.GetItem(itemId, Convert.ToString(Session["UserId"]));
            }
            catch (Exception e)
            {
                throw;
            }
            if (Convert.ToString(item.itemqty) == "0")
                item.itemqty = "1";
            ViewBag.qty = item.itemqty;
            return View(item);
        }

        [CheckSession]
        public ActionResult Cart()
        {
            Product product = new Product();
            List<ProductItems> items = new List<ProductItems>();

            items = product.GetCartList(Convert.ToString(Session["UserID"]));
            Session["tot"] = (Convert.ToInt32(items[0].itemqty) * Convert.ToDouble(items[0].salerate)).ToString("#.00");

            return View(items);
        }

        public ActionResult CheckOut(int? less)
        {
            sorder sorder = new sorder();
            try
            {
                var item = HttpContext.Session["Cart"] as ProductItems;

                Account account = new Account();
                Product product = new Product();
                var productItemBuyNow = new ProductItems();

                if (less == 1)
                {
                    TempData["less"] = "Suc";
                }
                else
                {
                    TempData["less"] = null;

                }

                var signupdate = account.GetUserDetails(Convert.ToString(Session["UserID"]));
                var productData = product.GetCartList(Convert.ToString(Session["UserID"]));
                TempData["address"]= addressManage.GetAddressList(Convert.ToString(Session["UserID"]));

                if (Session["BuyNow"] != null)
                {
                    productItemBuyNow = product.GetItem(Convert.ToString(Session["BuyNow"]), Convert.ToString(Session["UserId"]));
                }

                var currentSelectedAddressFrist = TempData["address"] as List<AddAddress>;
                if (currentSelectedAddressFrist.Count >0)
                {

                    sorder.address = currentSelectedAddressFrist[0].address;
                    sorder.mobileno = currentSelectedAddressFrist[0].mobileno;
                    sorder.emailid = signupdate.emailid;
                    sorder.customername = currentSelectedAddressFrist[0].name;
                    sorder.city = currentSelectedAddressFrist[0].city;
                    sorder.country = currentSelectedAddressFrist[0].country;
                    sorder.state = currentSelectedAddressFrist[0].state;
                    sorder.zipcode = currentSelectedAddressFrist[0].zipcode;
                }
                else
                {
                    sorder.address = "";
                    sorder.mobileno = "";
                    sorder.emailid = signupdate.emailid;
                    sorder.customername = "";
                    sorder.city = "";
                    sorder.country = "";
                    sorder.state = "";
                    sorder.zipcode = "";
                }
              

                if (Session["BuyNow"] == null)
                {
                    sorder.totamt = (Convert.ToInt32(productData[0].itemqty) * Convert.ToDouble(productData[0].salerate)).ToString("#.00");
                    sorder.itemname = productData[0].itemname;
                    sorder.itemunit = productData[0].unitname;
                    sorder.itemwt = productData[0].itemwt;
                    sorder.itemsalerate = productData[0].salerate;
                    sorder.itemqty = productData[0].itemqty;
                }
                else
                {
                    sorder.totamt = (Convert.ToInt32("1") * Convert.ToDouble(productItemBuyNow.salerate)).ToString("#.00");
                    sorder.itemname = productItemBuyNow.itemname;
                    sorder.itemunit = productItemBuyNow.unitname;
                    sorder.itemwt = productItemBuyNow.itemwt;
                    sorder.itemsalerate = productItemBuyNow.salerate;
                    sorder.itemqty = "1";
                    Session["tot"] = sorder.totamt;
                }

            }
            catch (Exception e)
            {
                throw;
            }
            return View(sorder);
        }

        [HttpPost]
        public ActionResult GetQuntity(ProductItems item)
        {
            Product product = new Product();
            item.custid = Convert.ToString(Session["UserID"]);
            var statusCode = product.AddCart(item);
            //Session["Cart"] = item;
            return RedirectToAction("Cart", "Product", new { area = "" });

        }

        [HttpPost]
        public ActionResult OrderPlaceProcess(sorder order)
        {
            if (order.address !="" && order.mobileno!="" && order.city!="" && order.address != null && order.mobileno != null && order.city != null )
            {
                Product product = new Product();
                var productData = product.GetCartList(Convert.ToString(Session["UserID"]));

                var productItemBuyNow = new ProductItems();

                if (Session["BuyNow"] != null)
                {
                    productItemBuyNow = product.GetItem(Convert.ToString(Session["BuyNow"]), Convert.ToString(Session["UserId"]));
                }


                if (productData != null || Session["BuyNow"] != null)
                {
                    order.custid = Convert.ToString(Session["UserID"]);
                    order.sodate = DateTime.UtcNow.ToString("yyyy/MM/dd");
                    order.sotime = DateTime.UtcNow.ToString("yyyy/MM/dd hh:mm");
                    order.taxtype = "0";
                    order.roffamt = "0";
                    order.discamt = "0";
                    if (Session["BuyNow"] == null)
                        order.totamt = (Convert.ToInt32(productData[0].itemqty) * Convert.ToDouble(productData[0].salerate)).ToString("#.00");
                    else
                        order.totamt = (Convert.ToInt32("1") * Convert.ToDouble(productItemBuyNow.salerate)).ToString("#.00");

                    order.advamt = "0";
                    order.deliverydate = DateTime.UtcNow.ToString("yyyy/MM/dd");
                    order.deliverycharge = "0";
                    order.deliverystatus = "0";
                    order.deliverytype = "0";
                    order.referenceno = "0";
                    order.ordstatus = "Pending";

                    sodtl sodetails = new sodtl();
                    if (Session["BuyNow"] == null)
                    {
                        sodetails.itemid = productData[0].itemid;
                        sodetails.unitid = productData[0].itemid;
                    }
                    else
                    {
                        sodetails.itemid = productItemBuyNow.itemid;
                        sodetails.unitid = productItemBuyNow.itemid;
                    }
                    sodetails.sgstamt = "0";
                    sodetails.cgstamt = "0";
                    sodetails.igstamt = "0";
                    sodetails.igstperc = "0";
                    sodetails.cgstperc = "0";
                    sodetails.sgstperc = "0";
                    sodetails.discamt = "0";
                    sodetails.discperc = "0";
                    if (Session["BuyNow"] == null)
                    {
                        sodetails.qty = productData[0].itemqty;
                        sodetails.grate = productData[0].salerate;
                        sodetails.netrate = productData[0].salerate;

                        sodetails.totamt = (Convert.ToInt32(productData[0].itemqty) * Convert.ToDouble(productData[0].salerate)).ToString("#.00");
                    }
                    else
                    {
                        sodetails.qty = productItemBuyNow.itemqty;
                        sodetails.grate = productItemBuyNow.salerate;
                        sodetails.netrate = productItemBuyNow.salerate;

                        sodetails.totamt = (Convert.ToInt32("1") * Convert.ToDouble(productItemBuyNow.salerate)).ToString("#.00");
                    }
                    sodetails.itemdeltime = DateTime.UtcNow.ToString("yyyy/MM/dd hh:mm");
                    sodetails.itmdeldate = DateTime.UtcNow.ToString("yyyy/MM/dd");

                    order.sodtls = new List<sodtl>();

                    order.sodtls.Add(sodetails);

                    var serializer = new JavaScriptSerializer();
                    string json = serializer.Serialize(order);

                    return RedirectToAction("MakePaymentEntry", "RozarPay", new { info = clsencr.Encrypt(json) });
                }
                else
                {
                    TempData["Man"] = "all fields are mandatory Please enter data";
                    return RedirectToAction("CheckOut");
                }
            }
            else
            {
                TempData["Man"] = "Please Enter  Delivery Address ";
                return  RedirectToAction("CheckOut");
            }

        }

        public ActionResult getOrder()
        {
            Product product = new Product();
            /* Customer Id as 11 fix */
            List<OrderEntity> data = product.GetOrder(Convert.ToString(Session["UserID"]));
            data= data.OrderByDescending(x => x.soid).ToList();
            return View(data);
        }

        public ActionResult CartRemove()
        {
            Product product = new Product();
            var status = product.ClearCart(Convert.ToString(Session["UserID"]));
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Invoice(string soid)
        {
            MemoryStream ms = new MemoryStream();
            Document doc = new Document(iTextSharp.text.PageSize.A4, 50, 60, 50, 60);
            PdfWriter pw = PdfWriter.GetInstance(doc, ms);

            PdfPTable tableParent = new PdfPTable(1);

            PdfPTable tablePageHeader = new PdfPTable(4);
            tableParent.TotalWidth = 80;

            PdfPCell cell; //this variable using all table

            Font arial = FontFactory.GetFont("Arial", 6);
            Font arial10 = FontFactory.GetFont("Arial", 10);

            //For Data All of Order
            Product product = new Product();
            List<OrderEntity> data = product.GetOrder(Convert.ToString(Session["UserID"]));
            var index = Convert.ToInt32(soid);

            //For Page Header

            Paragraph pageHeader = new Paragraph("Balram Pure Ghee", arial10);
            pageHeader.Alignment = Font.BOLD;
            pageHeader.Alignment = Element.ALIGN_RIGHT;
            pageHeader.IndentationRight = 50;


            Paragraph pageHeaderRight = new Paragraph("Organize By : Yash Enterprice \n Abhrampura, Taluka - Vijapur, District - Mahesana \n Gujarat - 384520,India \n Phone-8197937912  Email-balrampureghee@gmail.com ", arial);
            pageHeaderRight.Alignment = Element.ALIGN_RIGHT;
            pageHeaderRight.IndentationRight = 50;
            pageHeaderRight.SpacingAfter = 5;


            //For Page Header Image 
            string path = ControllerContext.HttpContext.Server.MapPath("~/Content/images/logo.png");
            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(path);
            image.ScaleToFit(80f, 40f);
            image.Alignment = Element.ALIGN_TOP;
            image.SetAbsolutePosition(95f, 745f);


            Paragraph inviceCreateDate = new Paragraph("Date :" + DateTime.UtcNow.Date.ToString("dd/MM/yyyy"));

            inviceCreateDate.SpacingAfter = 6;
            inviceCreateDate.Alignment = Font.BOLD;
            inviceCreateDate.Alignment = Element.ALIGN_RIGHT;

            //Document berfor wirte open it
            doc.Open();
            string reprotCenterName = "Invoice";

            //for Report wich type name display

            PdfPCell reprotName = new PdfPCell(new Phrase(reprotCenterName));
            reprotName.HorizontalAlignment = Font.BOLD;
            reprotName.HorizontalAlignment = Element.ALIGN_CENTER;


            //For Table Column
            int[] firstTablecellwidth = { 42, 10, 10, 10, 10 };

            //For Child Table

            PdfPTable tableChild = new PdfPTable(2);

            PdfPTable tableChildOne = new PdfPTable(5);

            PdfPTable tableOfTotal = new PdfPTable(2);

            tableChildOne.SetWidths(firstTablecellwidth);

            //For add table in add column
            var findData = data.Where(x => x.soid == soid).SingleOrDefault();
            cell = new PdfPCell(new Phrase("Customer Name  : " + findData.CustomerName, arial));
            cell.BorderWidth = 0;
            tableChild.AddCell(cell);

            cell = new PdfPCell(new Phrase("Bill No  : " + findData.soid, arial));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;

            tableChild.AddCell(cell);

            cell = new PdfPCell(new Phrase("Mobile  : " + findData.MobileNo, arial));
            cell.BorderWidth = 0;
            tableChild.AddCell(cell);

            cell = new PdfPCell(new Phrase("Date  : " + findData.sodate, arial));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            tableChild.AddCell(cell);

            cell = new PdfPCell(new Phrase("Address  : ", arial));
            cell.BorderWidth = 0;
            tableChild.AddCell(cell);

            /*------------------------------------------------------------------*/
            /*--------------- For Child table One -------------------------------*/



            cell = new PdfPCell(new Phrase("Product Name ", arial));
            cell.BorderWidth = 0;
            tableChildOne.AddCell(cell);

            cell = new PdfPCell(new Phrase("Rate ", arial));
            cell.BorderWidth = 0;
            tableChildOne.AddCell(cell);

            cell = new PdfPCell(new Phrase("Qty ", arial));
            cell.BorderWidth = 0;
            tableChildOne.AddCell(cell);

            cell = new PdfPCell(new Phrase("Dis(%) ", arial));
            cell.BorderWidth = 0;
            tableChildOne.AddCell(cell);

            cell = new PdfPCell(new Phrase("Amount ", arial));
            cell.BorderWidth = 0;
            tableChildOne.AddCell(cell);


            /*------------ For Child Table Data fill using Loop ----------------*/
            var orderDetails = product.GetOrderDetails(soid);


            foreach (var item in orderDetails)
            {
                PdfPCell cellChildOne; //this variable using all table

                cellChildOne = new PdfPCell(new Phrase(item.itemname, arial));
                cellChildOne.BorderWidth = 0;
                tableChildOne.AddCell(cellChildOne);


                cellChildOne = new PdfPCell(new Phrase(item.netrate, arial));
                cellChildOne.BorderWidth = 0;
                tableChildOne.AddCell(cellChildOne);

                cellChildOne = new PdfPCell(new Phrase(item.qty, arial));
                cellChildOne.BorderWidth = 0;
                tableChildOne.AddCell(cellChildOne);

                cellChildOne = new PdfPCell(new Phrase("0", arial));
                cellChildOne.BorderWidth = 0;

                tableChildOne.AddCell(cellChildOne);

                cellChildOne = new PdfPCell(new Phrase(Convert.ToString(item.totamt), arial));
                cellChildOne.BorderWidth = 0;
                tableChildOne.AddCell(cellChildOne);

            }

            /*-------------- End Chile Table One Data using loop ---------------*/


            /*--------------------------------------------------------------------*/

            /*==========------------start Using Total display-------------------==========*/
            cell = new PdfPCell(new Phrase("Amount in words : " + product.NumberToWords(Convert.ToInt32(findData.TotalAmt)), arial));
            cell.BorderWidth = 0;
            cell.Rowspan = 3;
            tableOfTotal.AddCell(cell);


            cell = new PdfPCell(new Phrase("Total Amount : " + Convert.ToString(findData.TotalAmt), arial));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            tableOfTotal.AddCell(cell);

            cell = new PdfPCell(new Phrase("Dis Amount :  0", arial));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            tableOfTotal.AddCell(cell);

            cell = new PdfPCell(new Phrase("Net Amount : " + Convert.ToString(findData.TotalAmt), arial));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            tableOfTotal.AddCell(cell);

            /*==========------------End using Total display -------------------==========*/







            //Add Child Table in Parent table
            cell = new PdfPCell(tableChild);
            cell.BorderWidthTop = 1;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            tableParent.AddCell(cell);

            cell = new PdfPCell(reprotName);
            cell.BorderWidthTop = 1;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            tableParent.AddCell(cell);


            //Add Chile One Table in parent table
            cell = new PdfPCell(tableChildOne);
            cell.BorderWidthTop = 1;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            tableParent.AddCell(cell);

            //Add  Table of total in parent table
            cell = new PdfPCell(tableOfTotal);
            cell.BorderWidthTop = 1;
            cell.BorderWidthBottom = 1;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            tableParent.AddCell(cell);


            //wite document by sequnce
            doc.Add(image);
            doc.Add(pageHeader);
            doc.Add(pageHeaderRight);
            doc.Add(tableParent);
            doc.Close();


            byte[] byt = ms.ToArray();
            ms = new MemoryStream();
            ms.Write(byt, 0, byt.Length);
            ms.Position = 0;
            return new FileStreamResult(ms, "application/pdf");

        }

    }


    
    
}