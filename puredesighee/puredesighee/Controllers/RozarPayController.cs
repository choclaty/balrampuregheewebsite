﻿using Newtonsoft.Json;
using puredesighee.DataAccessLayer;
using puredesighee.Models;
using Razorpay.Api;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace puredesighee.Controllers
{
    public class RozarPayController : Controller
    {
        clsencrdecr clsencr = new clsencrdecr();
        sorder objord = new sorder();
        RozarPay objentry = new RozarPay();
        Product product = new Product();
        // GET: RozarPay
        public string orderId;
        // GET: RozarPay
        public ActionResult MakePaymentEntry(string info)
        {
            DataTable dt = new DataTable();
            string key = "", secret = "", strcustid = "", amount = "", payee = "", emailid = "", mobileno = "", description = "";
            string id = "";
            Session["paymentinfo"] = info;

            dynamic data1 = (clsencr.Decrypt(info));
            objord = JsonConvert.DeserializeObject<sorder>(data1);

            strcustid = objord.custid;

            amount = objord.totamt;
            payee = objord.customername;
            emailid = objord.emailid;
            mobileno = objord.mobileno;
            description = objord.remarks;

            key = Key.merchantKey;
            secret = Key.secretkey;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            double finalamt_1 = 0;
            finalamt_1 = Convert.ToDouble(amount) * 100;
            Dictionary<string, object> input = new Dictionary<string, object>();
            input.Add("amount", finalamt_1); // this amount should be same as transaction amount
            input.Add("currency", "INR");
            input.Add("receipt", Convert.ToString(id));
            input.Add("payment_capture", 1);

            RazorpayClient client = new RazorpayClient(key, secret);

            Razorpay.Api.Order order = client.Order.Create(input);
            orderId = order["id"].ToString();
            objord.referenceno = orderId;
            TempData["orderid"] = orderId;

            TempData["payee"] = payee;
            TempData["emailid"] = emailid;
            TempData["mobileno"] = mobileno;
            TempData["amount"] = finalamt_1;
            TempData["key"] = key;
            TempData["description"] = description;

            TempData.Keep("orderid");
            return View("MakePaymentEntry");
            //return RedirectToAction("Index", "Home");
        }

        public ActionResult ChargeSummary(string paymentId)
        {
            //string paymentId = Request.Form["razorpay_payment_id"];

            if (Convert.ToString(Session["paymentinfo"]) != "")
            {
                //dynamic data1 = JObject.Parse(clsencr.Decrypt(Convert.ToString(Session["paymentinfo"])));
                dynamic data1 = (clsencr.Decrypt(Convert.ToString(Session["paymentinfo"])));
                objord = JsonConvert.DeserializeObject<sorder>(data1);
                objord.referenceno = paymentId;

                //save purchase entry if payment is success
                SavePurchaseHistory();

                Session["paymentinfo"] = null;

                Session["qty"] = null;
                Session["tot"] = null;
                Session["BuyNow"] = null;
                Session["ItemId"] = null;
             var status=   product.ClearCart(Session["UserID"].ToString());

            }
            return RedirectToAction("Index", "Home");
            //return Json(new
            //{
            //    retMessage = "Payment Success"
            //}, JsonRequestBehavior.AllowGet);
        }

        public  string SavePurchaseHistory()
        {
            string JSONString = string.Empty;

            string purchasetype = "", purchasedate = "", purchasetime = "", purchaseval = "", amount = "", paymentby = "", refno = "", custid = "", adduserid = "";

            purchasetype = "Dairy Products";
            purchasedate = DateTime.UtcNow.ToString("yyyy-MM-dd");
            purchasetime = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm");
            purchaseval = objord.totamt;
            amount = objord.totamt;
            paymentby = "Online";
            refno = objord.referenceno;
            custid = objord.custid;
            adduserid = objord.custid;

            string retsoid = objentry.SaveOrderEntry(objord);
            objentry.SendOrderNotification(retsoid,"Panding", custid,"","");
            objentry.SavePurchaseHistory(purchasetype, purchasedate, purchasetime, purchaseval, amount, retsoid, refno, custid, adduserid, paymentby);

            return "";
        }
    }
}