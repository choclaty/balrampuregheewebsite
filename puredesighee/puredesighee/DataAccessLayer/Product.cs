﻿using puredesighee.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace puredesighee.DataAccessLayer
{
    public class Product
    {
        public List<OrderEntity> GetOrder(string custid)
        {
            List<OrderEntity> OrderList = new List<OrderEntity>();

            DataTable DT = new DataTable();
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("spl_getcustoiderlist", con);
                cmd.Parameters.AddWithValue("@p_custid", custid);

              
                cmd.Parameters.Add("@p_errorcode", SqlDbType.BigInt);
                cmd.Parameters["@p_errorcode"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@p_errormessage", SqlDbType.VarChar, 100);
                cmd.Parameters["@p_errormessage"].Direction = ParameterDirection.Output;

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(DT);
                con.Close();

                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        OrderEntity Orderdata = new OrderEntity();

                        Orderdata.soid = Convert.ToString(DT.Rows[i]["soid"]);
                        Orderdata.socode = Convert.ToString(DT.Rows[i]["socode"]);
                        Orderdata.sodate = Convert.ToString(DT.Rows[i]["sodate"]);
                        Orderdata.sotime = Convert.ToString(DT.Rows[i]["sotime"]);
                        Orderdata.CustomerName = Convert.ToString(DT.Rows[i]["CustomerName"]);
                        Orderdata.MobileNo = Convert.ToString(DT.Rows[i]["MobileNo"]);
                        Orderdata.TotalAmt = Convert.ToDouble(DT.Rows[i]["totamt"]);
                        Orderdata.ordstatus = Convert.ToString(DT.Rows[i]["ordstatus"]);

                        OrderList.Add(Orderdata);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return OrderList;
        }

        public List<ProductItems> GetItems()
        {
            List<ProductItems> ItemsList = new List<ProductItems>();

            DataTable DT = new DataTable();
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("sp_getallitemdetails", con);

                cmd.Parameters.Add("@p_errorcode", SqlDbType.BigInt);
                cmd.Parameters["@p_errorcode"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@p_errormessage", SqlDbType.VarChar, 100);
                cmd.Parameters["@p_errormessage"].Direction = ParameterDirection.Output;

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(DT);
                con.Close();

                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        ProductItems items = new ProductItems();

                        items.itemid = Convert.ToString(DT.Rows[i]["itemid"]);
                        items.itemcatid = Convert.ToString(DT.Rows[i]["itemcatid"]);
                        items.itemdesc = Convert.ToString(DT.Rows[i]["itemdesc"]);
                        items.ingredients = Convert.ToString(DT.Rows[i]["ingredients"]);
                        items.nutritioninfo = Convert.ToString(DT.Rows[i]["nutritioninfo"]);
                        items.itembarcode = Convert.ToString(DT.Rows[i]["itembarcode"]);
                        items.itemname = Convert.ToString(DT.Rows[i]["itemname"]);
                        items.discper = Convert.ToString(DT.Rows[i]["discper"]);
                        items.salerate = Convert.ToString(DT.Rows[i]["salerate"]);
                        items.discrate = Convert.ToString(DT.Rows[i]["discrate"]);
                        items.sgstper = Convert.ToString(DT.Rows[i]["sgstper"]);
                        items.cgstper = Convert.ToString(DT.Rows[i]["cgstper"]);
                        items.igstper = Convert.ToString(DT.Rows[i]["igstper"]);
                        items.itemwt = Convert.ToString(DT.Rows[i]["itemwt"]);
                        items.unitname = Convert.ToString(DT.Rows[i]["unitname"]);
                        items.favitem = Convert.ToString(DT.Rows[i]["favitem"]);
                        items.validfor = Convert.ToString(DT.Rows[i]["validfor"]);
                        items.taxtype = Convert.ToString(DT.Rows[i]["taxtype"]);
                        items.itemqty = Convert.ToString(DT.Rows[i]["itemqty"]);
                        items.review = Convert.ToString(DT.Rows[i]["review"]);
                        
                        ItemsList.Add(items);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return ItemsList;
        }

        public ProductItems GetItem(string singleItemId,string userId)
        {
            //User wise so add parametere user login to do
            ProductItems Item = new ProductItems();

            DataTable DT = new DataTable();
            SqlConnection con = null;

            try { 
            
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("sp_GetItemDetails", con);
                cmd.Parameters.Add("@itemid", singleItemId);
                cmd.Parameters.Add("@p_userid", userId);
                cmd.Parameters.Add("@p_errorcode", SqlDbType.BigInt);
                cmd.Parameters["@p_errorcode"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@p_errormessage", SqlDbType.VarChar, 100);
                cmd.Parameters["@p_errormessage"].Direction = ParameterDirection.Output;

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(DT);
                con.Close();

                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                       

                        Item.itemid = Convert.ToString(DT.Rows[i]["itemid"]);
                        Item.itemcatid = Convert.ToString(DT.Rows[i]["itemcatid"]);
                        Item.itemdesc = Convert.ToString(DT.Rows[i]["itemdesc"]);
                        Item.ingredients = Convert.ToString(DT.Rows[i]["ingredients"]);
                        Item.nutritioninfo = Convert.ToString(DT.Rows[i]["nutritioninfo"]);
                        Item.itembarcode = Convert.ToString(DT.Rows[i]["itembarcode"]);
                        Item.itemname = Convert.ToString(DT.Rows[i]["itemname"]);
                        Item.discper = Convert.ToString(DT.Rows[i]["discper"]);
                        Item.salerate = Convert.ToString(DT.Rows[i]["salerate"]);
                        Item.discrate = Convert.ToString(DT.Rows[i]["discrate"]);
                        Item.sgstper = Convert.ToString(DT.Rows[i]["sgstper"]);
                        Item.cgstper = Convert.ToString(DT.Rows[i]["cgstper"]);
                        Item.igstper = Convert.ToString(DT.Rows[i]["igstper"]);
                        Item.itemwt = Convert.ToString(DT.Rows[i]["itemwt"]);
                        Item.unitname = Convert.ToString(DT.Rows[i]["unitname"]);
                        Item.favitem = Convert.ToString(DT.Rows[i]["favitem"]);
                        Item.validfor = Convert.ToString(DT.Rows[i]["validfor"]);
                        Item.taxtype = Convert.ToString(DT.Rows[i]["taxtype"]);
                        Item.itemqty = Convert.ToString(DT.Rows[i]["itemqty"]);
                        Item.review = Convert.ToString(DT.Rows[i]["review"]);

                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return Item;
        }

        public string AddCart(ProductItems item)
        {
            SqlConnection con = null;
                        try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());

                
                SqlCommand cmd = new SqlCommand("spl_AddToCart", con);
                cmd.Parameters.AddWithValue("@itemid", item.itemid);
                cmd.Parameters.AddWithValue("@userid", item.custid);

               
                    cmd.Parameters.AddWithValue("@ItemQty", item.itemqty);

                cmd.Parameters.Add("@p_errorcode", SqlDbType.Int);
                cmd.Parameters["@p_errorcode"].Direction = ParameterDirection.Output;

                cmd.Parameters.Add("@p_errormessage", SqlDbType.VarChar, 100);
                cmd.Parameters["@p_errormessage"].Direction = ParameterDirection.Output;

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                cmd.ExecuteNonQuery();

                return cmd.Parameters["@p_errorcode"].Value.ToString();

            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                con.Close();
            }

        }
        public List<ProductItems> GetCartList(string userId)
        {
            List<ProductItems> ItemsList = new List<ProductItems>();

            DataTable DT = new DataTable();
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("sp_cartItemList", con);
                cmd.Parameters.AddWithValue("@p_userid", userId);

                cmd.Parameters.Add("@p_errorcode", SqlDbType.BigInt);
                cmd.Parameters["@p_errorcode"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@p_errormessage", SqlDbType.VarChar, 100);
                cmd.Parameters["@p_errormessage"].Direction = ParameterDirection.Output;

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(DT);
                con.Close();

                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        ProductItems items = new ProductItems();

                        items.itemid = Convert.ToString(DT.Rows[i]["itemid"]);
                        items.itemcatid = Convert.ToString(DT.Rows[i]["itemcatid"]);
                        items.itemdesc = Convert.ToString(DT.Rows[i]["itemdesc"]);
                        items.ingredients = Convert.ToString(DT.Rows[i]["ingredients"]);
                        items.nutritioninfo = Convert.ToString(DT.Rows[i]["nutritioninfo"]);
                        items.itembarcode = Convert.ToString(DT.Rows[i]["itembarcode"]);
                        items.itemname = Convert.ToString(DT.Rows[i]["itemname"]);
                        items.discper = Convert.ToString(DT.Rows[i]["discper"]);
                        items.salerate = Convert.ToString(DT.Rows[i]["salerate"]);
                        items.discrate = Convert.ToString(DT.Rows[i]["discrate"]);
                        items.sgstper = Convert.ToString(DT.Rows[i]["sgstper"]);
                        items.cgstper = Convert.ToString(DT.Rows[i]["cgstper"]);
                        items.igstper = Convert.ToString(DT.Rows[i]["igstper"]);
                        items.itemwt = Convert.ToString(DT.Rows[i]["itemwt"]);
                        items.unitname = Convert.ToString(DT.Rows[i]["unitname"]);
                        items.validfor = Convert.ToString(DT.Rows[i]["validfor"]);
                        items.taxtype = Convert.ToString(DT.Rows[i]["taxtype"]);
                        items.itemqty = Convert.ToString(DT.Rows[i]["itemqty"]);
                        items.review = Convert.ToString(DT.Rows[i]["review"]);

                        ItemsList.Add(items);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return ItemsList;
        }

        public string ClearCart(string userId)
        {
            SqlConnection con = null;
            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());


                SqlCommand cmd = new SqlCommand("spl_clearcart", con);
                cmd.Parameters.AddWithValue("@p_userid", userId);

                cmd.Parameters.Add("@p_errorcode", SqlDbType.Int);
                cmd.Parameters["@p_errorcode"].Direction = ParameterDirection.Output;

                cmd.Parameters.Add("@p_errormessage", SqlDbType.VarChar, 100);
                cmd.Parameters["@p_errormessage"].Direction = ParameterDirection.Output;

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                cmd.ExecuteNonQuery();

                return cmd.Parameters["@p_errorcode"].Value.ToString();

            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                con.Close();
            }

        }

        public ProductItems getBuyNow(string itemId,string userId)
        {
            return null;
        }

        public List<OrderDetailsEntity> GetOrderDetails(string SoID)
        {
            List<OrderDetailsEntity> detailList = new List<OrderDetailsEntity>();

            DataSet DT = new DataSet();
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());

                SqlCommand cmd = new SqlCommand("spl_getorderdtl", con);
                cmd.Parameters.AddWithValue("@p_soid", SoID);
                cmd.CommandType = CommandType.StoredProcedure;
              
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(DT);
                con.Close();

                if (DT.Tables[1].Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Tables[1].Rows.Count; i++)
                    {
                        OrderDetailsEntity Details = new OrderDetailsEntity();

                        Details.itemname = Convert.ToString(DT.Tables[1].Rows[i]["itemname"]);
                        Details.itemcatname = Convert.ToString(DT.Tables[1].Rows[i]["itemcatname"]);
                        Details.unitname = Convert.ToString(DT.Tables[1].Rows[i]["unitname"]);
                        Details.qty = Convert.ToString(DT.Tables[1].Rows[i]["qty"]);
                        Details.netrate = Convert.ToString(DT.Tables[1].Rows[i]["netrate"]);
                        Details.totamt = Convert.ToString(DT.Tables[1].Rows[i]["totamt"]);

                        detailList.Add(Details);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return detailList;
        }
        

        public string NumberToWords(int number)
        {
            if (number == 0) { return "zero"; }
            if (number < 0) { return "minus " + NumberToWords(Math.Abs(number)); }
            string words = "";
            if ((number / 10000000) > 0) { words += NumberToWords(number / 10000000) + " Crore "; number %= 10000000; }
            if ((number / 100000) > 0) { words += NumberToWords(number / 100000) + " Lakh "; number %= 100000; }
            if ((number / 1000) > 0) { words += NumberToWords(number / 1000) + " Thousand "; number %= 1000; }
            if ((number / 100) > 0) { words += NumberToWords(number / 100) + " Hundred "; number %= 100; }
            if (number > 0)
            {
                if (words != "") { words += "and "; }
                var unitsMap = new[] { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
                var tensMap = new[] { "Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "seventy", "Eighty", "Ninety" };
                if (number < 20) { words += unitsMap[number]; }
                else { words += tensMap[number / 10]; if ((number % 10) > 0) { words += "-" + unitsMap[number % 10]; } }
            }
            return words;
        }
    }
}