﻿using puredesighee.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace puredesighee.DataAccessLayer
{
    public class AddressManage
    {
        public string SaveAddress(AddAddress address,string userid,int calltype)
        {
            SqlConnection con = null;
            string password = "";
            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());

                SqlCommand cmd = new SqlCommand("spl_AddAddress", con);
                cmd.Parameters.AddWithValue("@p_userid", userid);
                cmd.Parameters.AddWithValue("@p_Type", address.type);
                cmd.Parameters.AddWithValue("@p_FlatNo", address.flatno);
                cmd.Parameters.AddWithValue("@p_Address1", address.address);
                cmd.Parameters.AddWithValue("@p_City", address.city);
                     cmd.Parameters.AddWithValue("@p_State", address.state);
                cmd.Parameters.AddWithValue("@p_Country", address.country);
                cmd.Parameters.AddWithValue("@p_ZipCode", address.zipcode);
                cmd.Parameters.AddWithValue("@p_MobileNo", address.mobileno);
                cmd.Parameters.AddWithValue("@p_FullName", address.name);
                cmd.Parameters.AddWithValue("@p_id", address.id);
                cmd.Parameters.AddWithValue("@p_formmode", calltype);

                cmd.Parameters.Add("@p_errorcode", SqlDbType.Int);
                cmd.Parameters["@p_errorcode"].Direction = ParameterDirection.Output;

                cmd.Parameters.Add("@p_errormessage", SqlDbType.VarChar, 100);
                cmd.Parameters["@p_errormessage"].Direction = ParameterDirection.Output;

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                cmd.ExecuteNonQuery();
                
                return cmd.Parameters["@p_errorcode"].Value.ToString();

            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                con.Close();
            }

        }

        public List<AddAddress> GetAddressList(string userId)
        {
            List<AddAddress> AddressList = new List<AddAddress>();

            DataTable DT = new DataTable();
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("spl_listAddress", con);
                cmd.Parameters.AddWithValue("@p_userid", userId);

                cmd.Parameters.Add("@p_errorcode", SqlDbType.BigInt);
                cmd.Parameters["@p_errorcode"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@p_errormessage", SqlDbType.VarChar, 100);
                cmd.Parameters["@p_errormessage"].Direction = ParameterDirection.Output;

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(DT);
                con.Close();

                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        AddAddress items = new AddAddress();

                        items.id = Convert.ToString(DT.Rows[i]["id"]);
                        items.type = Convert.ToString(DT.Rows[i]["type"]);
                        items.flatno = Convert.ToString(DT.Rows[i]["flatno"]);
                        items.address = Convert.ToString(DT.Rows[i]["address1"]);
                        items.city = Convert.ToString(DT.Rows[i]["city"]);
                        items.state = Convert.ToString(DT.Rows[i]["state"]);
                        items.country = Convert.ToString(DT.Rows[i]["country"]);
                        items.zipcode = Convert.ToString(DT.Rows[i]["zipcode"]);
                        items.defaddress = Convert.ToString(DT.Rows[i]["defaddress"]);
                        items.mobileno = Convert.ToString(DT.Rows[i]["mobileno"]);
                        items.name = Convert.ToString(DT.Rows[i]["fullname"]);
                        AddressList.Add(items);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return AddressList;
        }

        public string DeleteAddressFindById( string userid, string id)
        {
            SqlConnection con = null;
            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());

                SqlCommand cmd = new SqlCommand("spl_removeaddress", con);
                cmd.Parameters.AddWithValue("@p_userid", userid);
                cmd.Parameters.AddWithValue("@p_id", id);
                cmd.Parameters.AddWithValue("@p_authkey", "");

                cmd.Parameters.Add("@p_errorcode", SqlDbType.Int);
                cmd.Parameters["@p_errorcode"].Direction = ParameterDirection.Output;

                cmd.Parameters.Add("@p_errormessage", SqlDbType.VarChar, 100);
                cmd.Parameters["@p_errormessage"].Direction = ParameterDirection.Output;

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                cmd.ExecuteNonQuery();

                return cmd.Parameters["@p_errorcode"].Value.ToString();

            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                con.Close();
            }

        }
    }
}