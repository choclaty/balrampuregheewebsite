﻿using puredesighee.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace puredesighee.DataAccessLayer
{
    public class Home
    {

        public string SaveEnquiry(Enquiry enquiry)
        {
            SqlConnection con = null;
            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());

                SqlCommand cmd = new SqlCommand("spl_saveenquirydetails", con);
                cmd.Parameters.AddWithValue("@p_userid",enquiry.cusid );
                cmd.Parameters.AddWithValue("@p_personname", enquiry.customername);
                cmd.Parameters.AddWithValue("@p_personemail", enquiry.email);
                cmd.Parameters.AddWithValue("@p_personcontact", enquiry.mobileno);
                cmd.Parameters.AddWithValue("@p_enqsubject", enquiry.subject);
                cmd.Parameters.AddWithValue("@p_enqdetail", enquiry.remark);

                cmd.Parameters.Add("@p_errorcode", SqlDbType.Int);
                cmd.Parameters["@p_errorcode"].Direction = ParameterDirection.Output;

                cmd.Parameters.Add("@p_errormessage", SqlDbType.VarChar, 100);
                cmd.Parameters["@p_errormessage"].Direction = ParameterDirection.Output;

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                cmd.ExecuteNonQuery();

                return cmd.Parameters["@p_errorcode"].Value.ToString();

            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                con.Close();
            }

        }
               
    }
}