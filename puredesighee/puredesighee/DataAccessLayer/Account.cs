﻿using puredesighee.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace puredesighee.DataAccessLayer
{
    public class Account
    {
        sendnotification objnoti = new sendnotification();
        clsencrdecr clsencr = new clsencrdecr();

        public string SaveSignUP(SignUp signUp)
        {
            SqlConnection con = null;
            string password = "";
            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());

                SqlCommand cmd = new SqlCommand("sp_usersignup", con);
                cmd.Parameters.AddWithValue("@p_firstname", signUp.customername);
                cmd.Parameters.AddWithValue("@p_city", signUp.city);
                cmd.Parameters.AddWithValue("@p_email", signUp.emailid);
                password = clsencr.Encrypt(signUp.password);
                cmd.Parameters.AddWithValue("@p_userpwd", password);
                cmd.Parameters.AddWithValue("@p_mobile", signUp.mobileno);

                cmd.Parameters.Add("@p_retid", SqlDbType.BigInt);
                cmd.Parameters["@p_retid"].Direction = ParameterDirection.Output;

                cmd.Parameters.Add("@p_errorcode", SqlDbType.Int);
                cmd.Parameters["@p_errorcode"].Direction = ParameterDirection.Output;

                cmd.Parameters.Add("@p_errormessage", SqlDbType.VarChar, 100);
                cmd.Parameters["@p_errormessage"].Direction = ParameterDirection.Output;

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                cmd.ExecuteNonQuery();
                if (Convert.ToString(cmd.Parameters["@p_errorcode"].Value) == "1")
                {
                    //for registartion sent notification
                    SendSignUpNotification(signUp, Convert.ToString(cmd.Parameters["@p_retid"].Value));
                }
                return cmd.Parameters["@p_errorcode"].Value.ToString();

            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                con.Close();
            }

        }

        public Login GetLoginData(string emailmo,string pwd)
        {
            DataTable DT = new DataTable();
            Login logindata = new Login();
            SqlConnection con = null;

            var isValideRegEmail = new Regex(@"([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z]+\.)+([a-zA-Z]{2,3})");
            bool isValidEmail = isValideRegEmail.IsMatch(emailmo);

            var isValideRegMo = new Regex(@"^[6-9]\d{9}$");
            bool isValidMo = isValideRegMo.IsMatch(emailmo);

            string password = "";


            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("sp_userlogin", con);
                cmd.CommandType = CommandType.StoredProcedure;
                if (isValidEmail)
                    cmd.Parameters.AddWithValue("@p_email", emailmo);
                if (isValidMo)
                    cmd.Parameters.AddWithValue("@p_mobileno", emailmo);
                if (pwd != null || pwd != null)
                {
                    password = clsencr.Encrypt(pwd);
                    cmd.Parameters.AddWithValue("@p_password", password);
                }

                cmd.Parameters.Add("@p_errorcode", SqlDbType.BigInt);
                cmd.Parameters["@p_errorcode"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@p_errormessage", SqlDbType.VarChar, 100);
                cmd.Parameters["@p_errormessage"].Direction = ParameterDirection.Output;

                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(DT);
                con.Close();
                if (DT.Rows.Count > 0)
                {
                    logindata.userid = Convert.ToInt32(DT.Rows[0]["userid"]);
                    logindata.username = Convert.ToString(DT.Rows[0]["username"]);
                    //logindata.userpwd = Convert.ToString(DT.Rows[0]["userpwd"]);

                    return logindata;
                }
                return logindata;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public SignUp GetUserDetails(string userId)
        {
            DataTable DT = new DataTable();
            SignUp signupdata = new SignUp();
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("sp_getuserprofile", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@p_retid", SqlDbType.BigInt);
                cmd.Parameters["@p_retid"].Direction = ParameterDirection.Output;


                cmd.Parameters.AddWithValue("@p_userid", userId);
                cmd.Parameters.Add("@p_errorcode", SqlDbType.BigInt);
                cmd.Parameters["@p_errorcode"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@p_errormessage", SqlDbType.VarChar, 100);
                cmd.Parameters["@p_errormessage"].Direction = ParameterDirection.Output;

                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(DT);
                con.Close();
                if (DT.Rows.Count > 0)
                {
                    signupdata.id = Convert.ToString(DT.Rows[0]["userid"]);
                    signupdata.emailid = Convert.ToString(DT.Rows[0]["emailid"]);
                    signupdata.customername = Convert.ToString(DT.Rows[0]["fullname"]);
                    signupdata.mobileno = Convert.ToString(DT.Rows[0]["mobileno"]);
                    signupdata.address = Convert.ToString(DT.Rows[0]["address"]);
                    signupdata.zipcode = Convert.ToString(DT.Rows[0]["zipcode"]);
                    signupdata.city = Convert.ToString(DT.Rows[0]["City"]);
                    signupdata.country = Convert.ToString(DT.Rows[0]["country"]);
                    signupdata.state = Convert.ToString(DT.Rows[0]["state"]);

                    return signupdata;
                }
                return signupdata;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public string userProfileUpdate(SignUp signup)
        {
            DataTable DT = new DataTable();
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("sp_userprofileupdate", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@p_retid", SqlDbType.BigInt);
                cmd.Parameters["@p_retid"].Direction = ParameterDirection.Output;


                cmd.Parameters.AddWithValue("@p_userid", signup.id);
                cmd.Parameters.AddWithValue("@p_firstname", signup.customername);
                cmd.Parameters.AddWithValue("@p_city", signup.city);
                cmd.Parameters.AddWithValue("@p_email", signup.emailid);
                cmd.Parameters.AddWithValue("@p_mobile", signup.mobileno);
                cmd.Parameters.AddWithValue("@p_address", signup.address);
                cmd.Parameters.AddWithValue("@p_zipcode", signup.zipcode);
                cmd.Parameters.AddWithValue("@p_country", signup.country);
                cmd.Parameters.AddWithValue("@p_state", signup.state);
                cmd.Parameters.Add("@p_errorcode", SqlDbType.BigInt);
                cmd.Parameters["@p_errorcode"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@p_errormessage", SqlDbType.VarChar, 100);
                cmd.Parameters["@p_errormessage"].Direction = ParameterDirection.Output;

                con.Open();
                cmd.ExecuteNonQuery();
                return cmd.Parameters["@p_errorcode"].Value.ToString();


            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public string SendForgotPasswordNotification(string email)
        {
            string getSMSTemplate = "", strsql = "", smstempstr = "", mobileno = "", password = "", userid = "", personName = "", dlt_te_id = "", totamt = "", itemaname = "", orgname = "";
            DataTable dtget, dtgetorder = new DataTable();
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());
                SqlCommand cmd = new SqlCommand("spl_forgotpasswordEmail", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@p_email", email);

                cmd.Parameters.Add("@p_errorcode", SqlDbType.BigInt);
                cmd.Parameters["@p_errorcode"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@p_errormessage", SqlDbType.VarChar, 100);
                cmd.Parameters["@p_errormessage"].Direction = ParameterDirection.Output;

                cmd.Parameters.Add("@p_retpwd", SqlDbType.VarChar, 100);
                cmd.Parameters["@p_retpwd"].Direction = ParameterDirection.Output;

                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dtgetorder);
                con.Close();

                if (dtgetorder.Rows.Count > 0)
                {
                    password = Convert.ToString(dtgetorder.Rows[0]["retpwd"]);
                    password = clsencr.Decrypt(password);

                    getSMSTemplate = "select * from createsmstemplate  where smstype = 'Forgot Password SMS' and smsfor = 'Forgot Password SMS'";


                    dtget = objnoti.FillDataTable(getSMSTemplate);

                    if (dtget.Rows.Count > 0)
                    {
                        smstempstr = Convert.ToString(dtget.Rows[0]["smseng"]);
                        smstempstr = smstempstr.Replace("$forgotpwd$", password);
                        dlt_te_id = Convert.ToString(dtget.Rows[0]["dlt_te_id"]);
                    }

                    if (Convert.ToString(smstempstr) != "")
                    {
                        if (email != "" && Convert.ToString(smstempstr.Trim()) != "")
                        {
                            //send email
                            objnoti.SendEmailThroughSendgrid(email, smstempstr, "Forgot Password", 0, "contact@puredesigheeofindia.co.in", "");
                        }
                    }
                }
                return Convert.ToString(cmd.Parameters["@p_errorcode"].Value);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void SendSignUpNotification(SignUp signup, string userid)
        {
            string getSMSTemplate = "", smstempstr = "", dlt_te_id = "", strsql = "", orgname = "", password = "";
            DataTable dtget, dtgetorder;
            try
            {
                strsql = " select orgname from registerinfo where infoid=" + 1 + ""; ;
                dtgetorder = objnoti.FillDataTable(strsql);


                if (signup != null && dtgetorder.Rows.Count > 0)
                {
                    orgname = Convert.ToString(dtgetorder.Rows[0]["orgname"]);

                    getSMSTemplate = "select * from createsmstemplate  where smstype = 'User Registration' and smsfor = 'Sign Up'";


                    dtget = objnoti.FillDataTable(getSMSTemplate);

                    if (dtget.Rows.Count > 0)
                    {
                        smstempstr = Convert.ToString(dtget.Rows[0]["smseng"]);
                        smstempstr = smstempstr.Replace("$customername$", signup.customername);
                        smstempstr = smstempstr.Replace("$orgnamename$", orgname);
                        smstempstr = smstempstr.Replace("$username$", signup.emailid);
                        smstempstr = smstempstr.Replace("$password$", signup.password);
                        dlt_te_id = Convert.ToString(dtget.Rows[0]["dlt_te_id"]);
                    }

                    if (Convert.ToString(smstempstr) != "")
                    {
                        if (Convert.ToString(signup.mobileno) != "" && Convert.ToString(smstempstr.Trim()) != "")
                        {
                            objnoti.sendsmsthrougmsg91("Registration successfully", smstempstr, Convert.ToString(signup.mobileno), userid, signup.customername, dlt_te_id);
                        }

                        if (Convert.ToString(signup.emailid) != "" && Convert.ToString(smstempstr.Trim()) != "")
                        {
                            //send email
                            objnoti.SendEmailThroughSendgrid(Convert.ToString(signup.emailid), smstempstr, "Registration successfully", 0, "contact@puredesigheeofindia.co.in", "");
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public string sendOTP(Login login)
        {
            SqlConnection con = null;
            string password = "";
            try
            {

                var otp = GenerateRandomNo();
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());

                SqlCommand cmd = new SqlCommand("spl_saveotploginhistory", con);
                cmd.Parameters.AddWithValue("@mobileno", login.mobile);
                cmd.Parameters.AddWithValue("@otp", otp);

                cmd.Parameters.Add("@retid", SqlDbType.BigInt);
                cmd.Parameters["@retid"].Direction = ParameterDirection.Output;

                cmd.Parameters.Add("@errorcode", SqlDbType.Int);
                cmd.Parameters["@errorcode"].Direction = ParameterDirection.Output;

                cmd.Parameters.Add("@errormessage", SqlDbType.VarChar, 100);
                cmd.Parameters["@errormessage"].Direction = ParameterDirection.Output;

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                cmd.ExecuteNonQuery();

                if (Convert.ToString(cmd.Parameters["@errorcode"].Value) == "1")
                {
                    var userData = new SignUp();
                    userData = GetUserDetails(Convert.ToString(cmd.Parameters["@retid"].Value));
                    SendOTPNotification(userData, Convert.ToString(cmd.Parameters["@retid"].Value),Convert.ToString(otp));

                }
                return cmd.Parameters["@errorcode"].Value.ToString();

            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                con.Close();
            }

        }

        public int GenerateRandomNo()
        {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max);
        }

        public string VarifyOTP(string otp,string mobileno)
        {
            SqlConnection con = null;
            try
            {

                con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());

                SqlCommand cmd = new SqlCommand("sp_verifyloginotp", con);
                cmd.Parameters.AddWithValue("@p_mobileno", mobileno);
                cmd.Parameters.AddWithValue("@p_otpno", otp);

                cmd.Parameters.Add("@p_errorcode", SqlDbType.Int);
                cmd.Parameters["@p_errorcode"].Direction = ParameterDirection.Output;

                cmd.Parameters.Add("@p_validotp", SqlDbType.DateTime);
                cmd.Parameters["@p_validotp"].Direction = ParameterDirection.Output;

                cmd.Parameters.Add("@p_errormessage", SqlDbType.VarChar, 100);
                cmd.Parameters["@p_errormessage"].Direction = ParameterDirection.Output;

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                cmd.ExecuteNonQuery();

                return Convert.ToString(cmd.Parameters["@p_validotp"].Value);

            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                con.Close();
            }

        }

        public void SendOTPNotification(SignUp signup, string userid,string otp)
        {
            string getSMSTemplate = "", smstempstr = "", dlt_te_id = "", strsql = "", orgname = "", password = "";
            DataTable dtget, dtgetorder;
            try
            {
                strsql = " select orgname from registerinfo where infoid=" + 1 + ""; ;
                dtgetorder = objnoti.FillDataTable(strsql);

                if (signup != null && dtgetorder.Rows.Count > 0)
                {
                    orgname = Convert.ToString(dtgetorder.Rows[0]["orgname"]);

                    getSMSTemplate = "select * from createsmstemplate  where smstype = 'OTP SMS' and smsfor = 'OTP SMS'";


                    dtget = objnoti.FillDataTable(getSMSTemplate);

                    if (dtget.Rows.Count > 0)
                    {
                        smstempstr = Convert.ToString(dtget.Rows[0]["smseng"]);
                        smstempstr = smstempstr.Replace("$otpno$", otp);
                        dlt_te_id = Convert.ToString(dtget.Rows[0]["dlt_te_id"]);
                    }

                    if (Convert.ToString(smstempstr) != "")
                    {
                        if (Convert.ToString(signup.mobileno) != "" && Convert.ToString(smstempstr.Trim()) != "")
                        {
                            objnoti.sendsmsthrougmsg91("OTP ", smstempstr, Convert.ToString(signup.mobileno), userid, signup.customername, dlt_te_id);
                        }

                        if (Convert.ToString(signup.emailid) != "" && Convert.ToString(smstempstr.Trim()) != "")
                        {
                            //send email
                            objnoti.SendEmailThroughSendgrid(Convert.ToString(signup.emailid), smstempstr, "OTP", 0, "contact@puredesigheeofindia.co.in", "");
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}