﻿using System.Web;
using System.Web.Optimization;

namespace puredesighee
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));
            bundles.Add(new ScriptBundle("~/bundles/scripts")
                    .Include("~/Scripts/plugins.js")
                .Include("~/Scripts/bootstrap.min.js")
                   .Include("~/Scripts/slick.min.js")
                    .Include("~/Scripts/owl-carousel.js")
                      .Include("~/Scripts/isotope.js")
                      .Include("~/Scripts/magnific-popup.js")
                     .Include("~/Scripts/jquery.ajaxchimp.min.js")
                     .Include("~/Scripts/stellar.min.js")
                      .Include("~/Scripts/main.js"
));

            bundles.Add(new ScriptBundle("~/bundles/js")
                      .Include("~/Content/js/plugins.js")
                .Include("~/Content/js/bootstrap.min.js")
                   .Include("~/Content/js/slick.min.js")
                    .Include("~/Content/js/owl-carousel.js")
                      .Include("~/Content/js/isotope.js")
                      .Include("~/Content/js/magnific-popup.js")
                     .Include("~/Content/js/jquery.ajaxchimp.min.js")
                     .Include("~/Content/js/stellar.min.js")
                      .Include("~/Content/js/main.js"
));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/animate.css",
                      "~/Content/css/hover.css",
                      "~/Content/css/magnific-popup.css",
                      "~/Content/css/owl.carousel.css",
                      "~/Content/css/slick.css",
                      "~/Content/css/fontawesome.min.css",
                      "~/Content/css/bootstrap.min.css",
                      "~/Content/css/normalize.css",
                      "~/Content/css/style.css",
                      "~/Content/css/responsive.css"
                      ));


        }
    }
}
